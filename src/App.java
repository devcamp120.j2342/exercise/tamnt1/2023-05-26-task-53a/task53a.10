
public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();

        System.out.println("get Area:" + circle1.getArea());
        System.out.println("get Circumference:" + circle1.getCircumference());

        Circle circle2 = new Circle(3.0);

        System.out.println("get Area:" + circle2.getArea());
        System.out.println("get Circumference:" + circle2.getCircumference());
    }

}